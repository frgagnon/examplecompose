import unittest

from codeAPI import bookAPI

class BasicTests(unittest.TestCase):

	def test_nothing(self):
		self.assertTrue(1==34, "I don't always test my code; but when I do, I do it in PRODUCTION.")

if __name__ == '__main__':
	unittest.main()
